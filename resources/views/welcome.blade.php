<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>GITO</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/style.css">

        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>

    <div class="wrapper" style="background-color: #faf8f8">

    <header style="display: block">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div class="container">
        <a class="navbar-brand" href="#">Gito</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        </div>
    </nav>
    </header>
        <section  style="padding-top:80px">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                       <h1> Here you can generate Shorten Url, to ease sharing !</h1>
                    </div>
                </div>
            </div>

        </section>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 my-3 p-3 bg-white rounded shadow-sm">
                        <h4 class="border-bottom border-gray pb-2 mb-0"> Generate Short URL</h4>
                        <p><strong>An Example:</strong> </p>
                        <p> <strong>Turn this url </strong> <br>
                            https://www.flipkart.com/philips-bt40-portable-bluetooth-speaker/p/itm96dabc0ad3580?pid=ACCEXGKFR79ZCVU7&lid=LSTACCEXGKFR79ZCVU7PH7DNE&marketplace=FLIPKART&srno=b_1_4&otracker=browse&fm=organic&iid=a44d4627-36be-45d6-85c7-8745643dd8e7.ACCEXGKFR79ZCVU7.SEARCH&ssid=kvxjt03xqo0000001591453787228
                        </p>
                        <p> <b>into this </b> <br>
                            https://cp.me/ESZDEPM344ty
                            </p>
                        <form action="/shortenUrl" method="post" id="urlForm">
                            <label for="url"><strong>Paste Original URL:</strong></label>
                            <div class="input-group">


                                <input type="text" name="url" class="form-control" id="url" placeholder="Ex : https://www.flipkart.com/audio-video/speakers/pr?sid=0pm,0o7&otracker=categorytree" required="">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-secondary">Generate Short url</button>
                                </div>
                            </div>
                            <div id="contact_success_msg" class="form-status-info " style="display: none;color: green" >
                                Information you shared was successfully submitted. Thank you, we'll get back to you soon!
                            </div>
                            <div id="contact_error_msg" class="form-status-info" style="display: none;color: red" >
                                RED

                            </div>
                        </form>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12 my-3 p-3 bg-white rounded shadow-sm">
                        <h4 class="border-bottom border-gray pb-2 mb-0">Recently generated Urls</h4>
                        <div id="recentUrls">
                            <div class="media text-muted pt-3">
                                <p class="media-body pb-3 mb-0 small border-bottom border-gray">
                                    <strong class="d-block text-gray-dark">https://cp.me/ESZDEPM344ty</strong>
                                    https://www.flipkart.com/philips-bt40-portable-bluetooth-speaker/p/itm96dabc0ad3580?pid=ACCEXGKFR79ZCVU7&lid=LSTACCEXGKFR79ZCVU7PH7DNE&marketplace=FLIPKART&srno=b_1_4&otracker=browse&fm=organic&iid=a44d4627-36be-45d6-85c7-8745643dd8e7.ACCEXGKFR79ZCVU7.SEARCH&ssid=kvxjt03xqo0000001591453787228
                                </p>
                            </div>
                            <div class="media text-muted pt-3">
                                <p class="media-body pb-3 mb-0 small border-bottom border-gray">
                                    <strong class="d-block text-gray-dark">https://cp.me/ESZDEPM344ty</strong>
                                    https://www.flipkart.com/philips-bt40-portable-bluetooth-speaker/p/itm96dabc0ad3580?pid=ACCEXGKFR79ZCVU7&lid=LSTACCEXGKFR79ZCVU7PH7DNE&marketplace=FLIPKART&srno=b_1_4&otracker=browse&fm=organic&iid=a44d4627-36be-45d6-85c7-8745643dd8e7.ACCEXGKFR79ZCVU7.SEARCH&ssid=kvxjt03xqo0000001591453787228
                                </p>
                            </div>
                            <div class="media text-muted pt-3">
                                <p class="media-body pb-3 mb-0 small border-bottom border-gray">
                                    <strong class="d-block text-gray-dark">https://cp.me/ESZDEPM344ty</strong>
                                    https://www.flipkart.com/philips-bt40-portable-bluetooth-speaker/p/itm96dabc0ad3580?pid=ACCEXGKFR79ZCVU7&lid=LSTACCEXGKFR79ZCVU7PH7DNE&marketplace=FLIPKART&srno=b_1_4&otracker=browse&fm=organic&iid=a44d4627-36be-45d6-85c7-8745643dd8e7.ACCEXGKFR79ZCVU7.SEARCH&ssid=kvxjt03xqo0000001591453787228
                                </p>
                            </div>
                            <div class="media text-muted pt-3">
                                <p class="media-body pb-3 mb-0 small border-bottom border-gray">
                                    <strong class="d-block text-gray-dark">https://cp.me/ESZDEPM344ty</strong>
                                    https://www.flipkart.com/philips-bt40-portable-bluetooth-speaker/p/itm96dabc0ad3580?pid=ACCEXGKFR79ZCVU7&lid=LSTACCEXGKFR79ZCVU7PH7DNE&marketplace=FLIPKART&srno=b_1_4&otracker=browse&fm=organic&iid=a44d4627-36be-45d6-85c7-8745643dd8e7.ACCEXGKFR79ZCVU7.SEARCH&ssid=kvxjt03xqo0000001591453787228
                                </p>
                            </div>

                        </div>



                    </div>
                </div>

            </div>


        </section>




    </div>
    <script src="/js/custom.js"></script>

    </body>
</html>
