

function paginateLink(i){
    $('#recentUrls').load('/urls?page='+i);

}


$(document).ready(function() {



    $('#recentUrls').load('/urls');



    var frm = $('#urlForm');
    frm.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {

                if (data.status === 500) {

                    $('#contact_error_msg').fadeIn(300);
                    $('#contact_error_msg').text(data.data);


                } else {
                    $('#contact_success_msg').fadeIn(300);
                    $('#contact_success_msg').text(data.data);
                    $('#recentUrls').load('/urls');


                }

                setTimeout(function () {
                    $('#contact_error_msg').fadeOut(400);
                }, 6000);


                setTimeout(function () {
                    $('#contact_success_msg').fadeOut(300);
                }, 8000);

                $("#urlForm")[0].reset();
            },
            error: function (data) {
                if (data.status === 422) {
                    $('#contact_captcha_msg').fadeIn(400)
                } else {
                    $('#contact_error_msg').fadeIn(400);
                    $('#contact_error_msg').text(data.data);

                    setTimeout(function () {
                        $('#contact_error_msg').fadeOut(400);
                    }, 6000);
                }
            },
        });
    });
});
