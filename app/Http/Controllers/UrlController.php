<?php

namespace App\Http\Controllers;

use App\Url;
use Illuminate\Http\Request;

class UrlController extends Controller
{
    public function index(){


        return view('welcome');

    }
    public function urls(){

        $urls=Url::orderBy('id','desc')->paginate(5);

        foreach($urls as $url){
            echo ' <div class="media text-muted pt-3">
                                <p style="overflow:auto" class="media-body pb-3 mb-0 small border-bottom border-gray">
                                    <strong class="d-block text-gray-dark"><a href='.$url['shorten_url'].' >'.$url['shorten_url'].'</a></strong>
                                    '.$url['original_url'].'
                                </p>
                            </div>';
        }

       echo '<small class="d-block text-right mt-3">';
        for ($i=1;$i<$urls->lastPage();$i++){
            if($urls->currentPage()==$i){
                echo ' <u> <a  class="paginateLink" data-no="'.$i.'"  onclick="paginateLink('.$i.')"  href="javascript:void(0)" >'.$i.'</a></u>';
            }else{
                echo ' <a class="paginateLink" data-no="'.$i.'" onclick="paginateLink('.$i.')" href="javascript:void(0)" >'.$i.'</a>';
            }
        }
        echo '</small>';


    }
    public function shortenUrl(Request $request){

        $url = $request['url'];

        //1.Validate url

        if(strpos($url,'http://')===false){
            if(strpos($url,'https://')===false){
                $url='http://'.$url;
            }
        }

        $valid=$this->validateUrl($url);

        if(!$valid){
            return Response()->json([
                'data'=>'Invalid URL Format',
                'status'=>500,
                'message'=>'Invalid URL Format',
            ]);
        }

        //2. Verify url exists
        $verify=$this->verifyUrlExists($url);
        if(!$verify){
            return Response()->json([
                'data'=>'URL does not exists',
                'status'=>500,
                'message'=>'URL does not exists',
            ]);
        }

        //3. Url exists in DB
        $exists= Url::where('original_url',$url)->first();

        if($exists){
            return Response()->json([
                'data'=>'URL already exists with shorten url: '.$exists['shorten_url'] ,
                'status'=>200,
                'message'=>'URL does not exists',
            ]);
        }


        //4. Generate short Url
        $savedUrl=Url::create([
            'original_url'=>$url,
        ]);


        //5.Return Success with shortenUrl

        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $baseurl='https://cp.me/';
        $savedUrl->shorten_url=$baseurl.substr(str_shuffle($permitted_chars),0,6).$savedUrl->id;
        $savedUrl->save();
        return Response()->json([
            'data'=>'Success!.Shorten url: '.$savedUrl['shorten_url'] ,
            'status'=>200,
            'message'=>'Success',
        ]);


    }

    public function validateUrl($url){
        return filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOSTNAME);

    }
    public function verifyUrlExists($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch,  CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return (!empty($response) && $response != 404);
    }


}
