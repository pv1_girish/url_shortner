-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 07, 2020 at 08:48 PM
-- Server version: 5.7.29-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-5+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tinyurl`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `urls`
--

CREATE TABLE `urls` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `original_url` text,
  `shorten_url` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `urls`
--

INSERT INTO `urls` (`id`, `created_at`, `updated_at`, `original_url`, `shorten_url`) VALUES
(12, '2020-06-06 16:54:08', '2020-06-06 16:54:08', 'http://flipkart.co.in', 'https://cp.me/Dmo8YF12'),
(13, '2020-06-07 11:20:11', '2020-06-07 11:20:11', 'http://facebook.com', 'https://cp.me/jlukBG13'),
(14, '2020-06-07 12:39:27', '2020-06-07 12:39:27', 'http://test.io', 'https://cp.me/8EvXGN14'),
(15, '2020-06-07 12:40:39', '2020-06-07 12:40:39', 'http://gmail.com', 'https://cp.me/ydPNTo15'),
(16, '2020-06-07 14:59:31', '2020-06-07 14:59:31', 'https://www.flipkart.com/philips-bt40-portable-bluetooth-speaker/p/itm96dabc0ad3580?pid=ACCEXGKFR79ZCVU7&lid=LSTACCEXGKFR79ZCVU7PH7DNE&marketplace=FLIPKART&srno=b_1_4&otracker=browse&fm=organic&iid=a44d4627-36be-45d6-85c7-8745643dd8e7.ACCEXGKFR79ZCVU7.SEARCH&ssid=kvxjt03xqo0000001591453787228', 'https://cp.me/Ta2L5H16'),
(17, '2020-06-07 15:00:02', '2020-06-07 15:00:02', 'https://www.amazon.in/s?k=pillows+for+sleeping&i=kitchen&s=price-asc-rank&page=7&crid=11JX59FMNG01L&qid=1571913295&sprefix=pillow%2Ckitchen%2C317&ref=sr_pg_7', 'https://cp.me/EGH2Yx17'),
(18, '2020-06-07 15:00:36', '2020-06-07 15:00:36', 'https://www.amazon.in/b/ref=s9_acss_bw_cg_hsssbc20_2c1_w?node=14839093031&pf_rd_m=A1K21FY43GMZF8&pf_rd_s=merchandised-search-4&pf_rd_r=S5SMGDC2GFVR810BYJDB&pf_rd_t=101&pf_rd_p=d3de6fa4-6946-4369-b6ce-e58824badbb1&pf_rd_i=976442031', 'https://cp.me/UjfYgF18'),
(19, '2020-06-07 15:01:26', '2020-06-07 15:01:26', 'https://www.flipkart.com/audio-video/pr?sid=0pm&marketplace=FLIPKART&offer=nb:mp:0526449005,nb:mp:0551747205&fm=neo%2Fmerchandising&iid=M_6cc1f46e-e93b-4235-b7a3-d8914810b435_3.ZM6UJ6TGOCD2&ssid=b6wh28x5680000001591542056810&otracker=hp_omu_Deals%2Bof%2Bthe%2BDay_1_3.dealCard.OMU_Deals%2Bof%2Bthe%2BDay_ZM6UJ6TGOCD2_2&otracker1=hp_omu_SECTIONED_neo%2Fmerchandising_Deals%2Bof%2Bthe%2BDay_NA_dealCard_cc_1_NA_view-all_2&cid=ZM6UJ6TGOCD2', 'https://cp.me/AvFWRD19'),
(20, '2020-06-07 15:02:26', '2020-06-07 15:02:26', 'https://www.flipkart.com/staunch-flex-100-bluetooth-headset/p/itme32ab1eb0fc84?pid=ACCFMXVKPV9ZHXW4&lid=LSTACCFMXVKPV9ZHXW4K01QFZ&marketplace=FLIPKART&srno=b_1_2&otracker=hp_omu_Deals%2Bof%2Bthe%2BDay_1_3.dealCard.OMU_Deals%2Bof%2Bthe%2BDay_ZM6UJ6TGOCD2_2&otracker1=hp_omu_SECTIONED_neo%2Fmerchandising_Deals%2Bof%2Bthe%2BDay_NA_dealCard_cc_1_NA_view-all_2&fm=neo%2Fmerchandising&iid=885609f9-639c-4762-91f2-ea83d885b8ad.ACCFMXVKPV9ZHXW4.SEARCH&ppt=browse&ppn=browse&ssid=moc6jqxr8g0000001591542075801', 'https://cp.me/M640Jc20'),
(21, '2020-06-07 15:14:35', '2020-06-07 15:14:36', 'https://www.flipkart.com/boat-rugby-10-w-portable-bluetooth-speaker/p/itmd4133d3368575?pid=ACCEJFU3BTM5ZWHG&lid=LSTACCEJFU3BTM5ZWHGJAXZHT&marketplace=FLIPKART&srno=b_1_4&otracker=hp_omu_Deals%2Bof%2Bthe%2BDay_1_3.dealCard.OMU_Deals%2Bof%2Bthe%2BDay_ZM6UJ6TGOCD2_2&otracker1=hp_omu_SECTIONED_neo%2Fmerchandising_Deals%2Bof%2Bthe%2BDay_NA_dealCard_cc_1_NA_view-all_2&fm=neo%2Fmerchandising&iid=885609f9-639c-4762-91f2-ea83d885b8ad.ACCEJFU3BTM5ZWHG.SEARCH&ppt=browse&ppn=browse', 'https://cp.me/GgKuEx21'),
(22, '2020-06-07 15:14:53', '2020-06-07 15:14:53', 'https://www.amazon.in/iBall-Slide-Dazzle-i7-Midnight/dp/B07D77V1DX/ref=sr_1_17?s=computers&ie=UTF8&qid=1549957527&sr=1-17', 'https://cp.me/wWUHi322'),
(23, '2020-06-07 15:15:14', '2020-06-07 15:15:14', 'https://www.amazon.in/b/ref=gwdb_bmc_1_CP_Latest_RedmiNote9Pro?_encoding=UTF8&node=21021782031&pf_rd_s=merchandised-search-5&pf_rd_t=Gateway&pf_rd_i=mobile&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_r=BNFC8DB5Z8T3B5F7RD77&pf_rd_p=d381c180-112f-4298-bada-8b68f52335d7', 'https://cp.me/pc1Puh23'),
(24, '2020-06-07 15:15:31', '2020-06-07 15:15:31', 'https://www.amazon.in/b/ref=s9_acss_bw_cg_CPACS_2a1_w?node=14453538031&pf_rd_m=A1K21FY43GMZF8&pf_rd_s=merchandised-search-8&pf_rd_r=BNFC8DB5Z8T3B5F7RD77&pf_rd_t=101&pf_rd_p=3b6413f1-424f-448e-b56c-409860ffe8c7&pf_rd_i=1389401031', 'https://cp.me/EPlot724'),
(25, '2020-06-07 15:16:09', '2020-06-07 15:16:10', 'https://www.flipkart.com/boat-rockerz-510-super-extra-bass-bluetooth-headset/p/itmd55bd7abda205?pid=ACCF5GVPFF3Z6VQY&lid=LSTACCF5GVPFF3Z6VQYY1CB7K&marketplace=FLIPKART&srno=b_1_5&otracker=hp_omu_Deals%2Bof%2Bthe%2BDay_1_3.dealCard.OMU_Deals%2Bof%2Bthe%2BDay_ZM6UJ6TGOCD2_2&otracker1=hp_omu_SECTIONED_neo%2Fmerchandising_Deals%2Bof%2Bthe%2BDay_NA_dealCard_cc_1_NA_view-all_2&fm=neo%2Fmerchandising&iid=885609f9-639c-4762-91f2-ea83d885b8ad.ACCF5GVPFF3Z6VQY.SEARCH&ppt=browse&ppn=browse&ssid=moc6jqxr8g0000001591542075801', 'https://cp.me/uWD9Qa25'),
(26, '2020-06-07 15:16:54', '2020-06-07 15:16:54', 'https://www.google.co.in/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png', 'https://cp.me/jWLDp926'),
(27, '2020-06-07 15:17:33', '2020-06-07 15:17:33', 'https://www.flipkart.com/televisions/pr?sid=ckf%2Cczl&p%5B%5D=facets.availability%255B%255D%3DExclude%2BOut%2Bof%2BStock&otracker=categorytree&p%5B%5D=facets.serviceability%5B%5D%3Dtrue&p%5B%5D=facets.brand%255B%255D%3DSamsung&otracker=nmenu_sub_TVs%20%26%20Appliances_0_Samsung', 'https://cp.me/rjLTxh27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `urls`
--
ALTER TABLE `urls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `urls`
--
ALTER TABLE `urls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
